# G213-Setup

To document the requirements needed to configure g810-led and get it configured the way I want to use it. 

## Desired effect

Set static color on bootup.  When the system is locked I want the backlight to disappear.  And I want the backlight to come back when I log back in.

## Situation

- Have a Logitech G Series Keyboard
- Be on linux where G Hub doesn't exist.


## Requirements

- Install G810-Led [g810-led](https://github.com/MatMoul/g810-led) according to their instructions.

## Permissions

In order to run the above command you need to have Sudo rights. Assuming that you want to allow people to change the color of the keyboard you can give sudo rights to just a that one command. 

```bash
sudo groupadd g213
sudo usermod -aG g213 userid
sudo visudo -f /etc/sudoers.d/1-g213-keyboard-led

    %g213    ALL=(root)    NOPASSWD:/usr/bin/g213-led
```

Once that lines been added to sudoers file you will need to log out of all sessions and log back in.
This will allow and user in that group to run that one binary as sudo.

## Other setup

We need to create any of our own profiles to be used and set a couple other things up.
```bash
# move the two profiles to the profile directory.
sudo cp ./profile.g213.lock /etc/g810-led/
sudo cp ./profile.g213.login /etc/g810-led/

# move the rules to udev rules directory.  (This endsures that on startup it will apply a color to the keyboard)
sudo cp ./g213-led.rules /etc/udev/rules.d/

# Alternatively you could modify the one that is already present there from the g810-led install.
# Or add the command to the ~/.bashrc and ~/bash_logout to set it on login and logout. 

# finally copy the serivce to the systemd user directory.
sudo cp ./g213-keyboard.service /etc/systemd/user/
```

Due to permissions with the dbus-monitor access it should be accessed by the present user.
The next trick is to make sure you're connected to the correct dbus pipe. 
This will require modifying the keyboard.sh script to find your desktop .

- ```gnome-session``` is the one used for Gnome desktops and variants. 
- I was using budgie at the time so I needed to look for ```budgie-wm```

This exports the location of the users dbus address which we can then attach to as the service. 

For KDE there might be differences, also this will only find the first so if you run the ./keyboard.sh from your local shell it should print a warning and nothing else.  If it prints more it's because it doesn't have permission and likely found the wrong session. 

## Final steps. 

Reload systemctl and start/enable your new service. 
Now when you lock you system it should set the backlight off on the keyboard and resume when you log back in.

```bash
systemctl --user daemon-reload
systemctl --user start g213-keyboard.service
systemctl --user enable g213-keyboard.service
```

