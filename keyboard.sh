#! /usr/bin/env bash

DBUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pidof -s budgie-wm)/environ | sed 's/DBUS_SESSION_BUS_ADDRESS=//g')
dbus-monitor --address $DBUS_ADDRESS "type='signal',interface='org.gnome.ScreenSaver'" |
while read x; do
    case "$x" in 
        *"boolean true"*) sudo /usr/bin/g213-led -p /etc/g810-led/profile.g213.lock; /usr/bin/numlockx off;;
        *"boolean false"*) sudo /usr/bin/g213-led -p /etc/g810-led/profile.g213.login; /usr/bin/numlockx on;;
    esac
done
